package ru.chelnokov.chickenOrEgg;

/**
 * Класс для создания и запуска потоков
 */

public class Task {
    public static void main(String[] args) throws InterruptedException {
        ThreadSon chicken = new ThreadSon("-Курица!");
        ThreadSon egg = new ThreadSon("-Яйцо!");

        String win = "";
        chicken.start();
        egg.start();
        while (egg.isAlive() | chicken.isAlive()){
            if (egg.isAlive() & !chicken.isAlive()){
                win = "Яйцо появилось первым (а вы не верили да)";
            }
            if (!egg.isAlive() & chicken.isAlive()){
                win = "Chicken power!!";
            }
        }
        egg.join();
        chicken.join();
        System.out.println(win);
        System.out.println("Спор закончен");
    }
}
